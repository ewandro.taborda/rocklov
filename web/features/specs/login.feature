#language: pt

Funcionalidade: Login do Usuário

    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenário: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais "user@rocklov.com.br" e "pwd123"
        Então sou redirecionado para o Dashboard

    
    Esquema do Cenário: Tentativa de Login

        Dado que acesso a página principal
        Quando submeto minhas credenciais "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input          | senha_input | mensagem_output                  |
            | admin@rocklov.com.br | abc123      | Usuário e/ou senha inválidos.    |
            | admin@404.com.br     | abc123      | Usuário e/ou senha inválidos.    |
            | admin*rocklov.com.br | abc123      | Oops. Informe um email válido!   |
            |                      | abc123      | Oops. Informe um email válido!   |
            | admin@rocklov.com.br |             | Oops. Informe sua senha secreta! |