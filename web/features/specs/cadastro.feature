#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenário: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome                 | email                     | senha  |
            | Ewandro Luiz Taborda | ewandro.taborda@gmail.com | pwd123 |
        Então sou redirecionado para o Dashboard


    Esquema do Cenário:
        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input           | email_input               | senha_input | mensagem_output                  |
            |                      | ewandro.taborda@gmail.com | pwd123      | Oops. Informe seu nome completo! |
            | Ewandro Luiz Taborda |                           | pwd123      | Oops. Informe um email válido!   |
            | Ewandro Luiz Taborda | email$invalido.com.br     | pwd123      | Oops. Informe um email válido!   |
            | Ewandro Luiz Taborda | email*invalido.com.br     | pwd123      | Oops. Informe um email válido!   |
            | Ewandro Luiz Taborda | ewandro.taborda@gmail.com |             | Oops. Informe sua senha secreta! |



# Cenario: Submeter cadastro sem o nome

#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         | nome | email                     | senha  |
#         |      | ewandro.taborda@gmail.com | pwd123 |
#     Então vejo a mensagem de alerta: "Oops. Informe seu nome completo!"


# Cenario: Submeter cadastro sem o email

#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         | nome                 | email | senha  |
#         | Ewandro Luiz Taborda |       | pwd123 |
#     Então vejo a mensagem de alerta: "Oops. Informe um email válido!"


# Cenario: Submeter cadastro com email incorreto

#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         | nome                 | email                 | senha  |
#         | Ewandro Luiz Taborda | email$invalido.com.br | pwd123 |
#     Então vejo a mensagem de alerta: "Oops. Informe um email válido!"

# Cenario: Submeter cadastro sem a senha

#     Dado que acesso a página de cadastro
#     Quando submeto o seguinte formulário de cadastro:
#         | nome                 | email                     | senha |
#         | Ewandro Luiz Taborda | ewandro.taborda@gmail.com |       |
#     Então vejo a mensagem de alerta: "Oops. Informe sua senha secreta!"