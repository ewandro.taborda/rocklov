#language: pt

Funcionalidade: Remover Anuncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero pode remover esse anúncio
    Para que possa manter meu dashboard atualizado

    Contexto: Login
        * Login com "spider@rocklov.com.br" e "pwd123"

    
    Cenario: Remover um anúncio

        Dado que eu tenho um anúncio indesejado:
            | thumb     | telecaster.jpg |
            | nome      | Telecaster     |
            | categoria | Cordas         |
            | preco     | 50             |
        Quando eu solicito a exclusão desse item
            E confirmo a exclusão
        Então não devo ver esse item item no meu dashboard

    
    Cenario: Desistir da Exclusão

        Dado que eu tenho um anúncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outros    |
            | preco     | 100       |
        Quando eu solicito a exclusão desse item
            Mas não confirmo a solicitação
        Então esse item deve permanecer no meu Dashboard