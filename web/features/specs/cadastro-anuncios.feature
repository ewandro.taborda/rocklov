#language: pt

Funcionalidade: Cadastro de Anúncios
    Sendo usuário cadastrado no Rocklov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Contexto: Login
        * Login com "admin@rocklov.com.br" e "pwd123"

    Cenário: Novo Equipamento

        Dado que acesso o formulário de cadastro de anúncios
            E que eu tenho o seguinte equipamento
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submeto o cadastro desse item
        Então devo ver esse item no meu Dashboard

    @tentativa
    Esquema do Cenário: Tentativa de Cadastro de Anúncios
        Dado que acesso o formulário de cadastro de anúncios
            E que eu tenho o seguinte equipamento
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submeto o cadastro desse item
        Então deve conter a mensagem de alerta: "<mensagem>"

        Exemplos:
            | foto          | nome      | categoria | preco | mensagem                             |
            |               | Violão    | Cordas    | 99    | Adicione uma foto no seu anúncio!    |
            | clarinete.jpg |           | Outros    | 120   | Informe a descrição do anúncio!      |
            | mic.jpg       | Microfone |           | 100   | Informe a categoria                  |
            | baixo.jpg     | Baixo     | Cordas    |       | Informe o valor da diária            |
            | conga.jpg     | Conga     | Outros    | abc   | O valor da diária deve ser numérico! |
            | sanfona.jpg   | Sanfona   | Outros    | 100c  | O valor da diária deve ser numérico! |
