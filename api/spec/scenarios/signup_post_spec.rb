describe "POST /signup" do
  context "Novo Usuário" do
    before(:all) do
      payload = { name: "New User", email: "newuser@gmail.com", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end
    it "Validação do Status Code" do
      expect(@result.code).to eql 200
    end

    it "Validação do ID do Usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "Usuário Duplicado" do
    before(:all) do
      payload = { name: "Duplicate User", email: "duplicate@gmail.com", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      Signup.new.create(payload)
      @result = Signup.new.create(payload)
    end

    it "Validação do Status Code - 409" do
      expect(@result.code).to eql 409
    end

    it "Validação da Mensagem - Email already exists :(" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end
end
