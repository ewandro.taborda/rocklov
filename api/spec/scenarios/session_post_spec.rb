describe "POST /sessions" do
  context "Login com Sucesso" do
    before(:all) do
      payload = { email: "admin@hotmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
    end

    it "Validação do Status Code" do
      expect(@result.code).to eql 200
    end

    it "Validação do ID do Usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  # examples = [
  #   {
  #     title: "Senha Incorreta",
  #     payload: { email: "admin@rocklov.com.br", password: "invalida" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Usuário Inexistente",
  #     payload: { email: "404@rocklov.com.br", password: "invalida" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Email em Branco",
  #     payload: { email: "", password: "invalida" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Sem Campo Email",
  #     payload: { password: "invalida" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Senha em Branco",
  #     payload: { email: "admin@rocklov.com.br", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Sem Campo Senha",
  #     payload: { email: "admin@rocklov.com.br" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "Validação do Status Code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "Validação da Mensagem de Erro - #{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
